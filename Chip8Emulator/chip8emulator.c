#include <stdio.h>
#include <stdlib.h>
#include <io.h>  
#include <string.h>
#include "SDLData.h"
#include "chip8.h"

#define WIDTH 64
#define HEIGHT 32
#define SCALEFACTOR 15
#define SCALEDWIDTH (WIDTH * SCALEFACTOR)
#define SCALEDHEIGHT (HEIGHT * SCALEFACTOR)
#define ERROR_MSG_BUFFER_LEN 128

// Window position
#define POSX 100
#define POSY 100

/*
* Sets up SDL window and surface for graphics
*/
void graphics_init(struct SDLDATA **SDLData, int posX, int posY,
	int width, int height);

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		printf("Too few or too many arguments provided!\nUsage: chip8emulator.c <programname>.<extension>");
		exit(EXIT_FAILURE);
	}

	struct SDLDATA *SDLData = malloc(sizeof(struct SDLDATA));
	SDL_Event event;

	if (SDLData == NULL)
	{
		printf("Could not create struct for SDLData!\n");
		exit(EXIT_FAILURE);
	}

	graphics_init(&SDLData, POSX, POSY, SCALEDWIDTH, SCALEDHEIGHT);

	chip8_init();
	chip8_load_game(argv[1]);

	for (;;)
	{
		chip8_emulate_cycle();
		
		if (chip8_drawflag())
			chip8_draw(SDLData, WIDTH, HEIGHT, SCALEDWIDTH, SCALEDHEIGHT);
			
		chip8_loadkeys();

		// User wants to close applicattion
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				SDL_Quit();
				return 0;
			}
		}
	} 
}

void graphics_init(struct SDLDATA **SDLData, int posX, int posY,
	int width, int height)
{
	char error_msg[ERROR_MSG_BUFFER_LEN];

	SDL_Window **win = &((*SDLData)->win);
	SDL_Surface **winSurface = &((*SDLData)->winSurface);
	SDL_Surface **drawSurface = &((*SDLData)->drawSurface);

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("Could not initialize SDL: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	// Need window and surface for the pixel data
	*win = SDL_CreateWindow("CHIP8 Emulator", posX, posY, width, height, 0);

	if (*win == NULL)
	{
		printf("Could not create window: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}

	*winSurface = SDL_GetWindowSurface(*win);

	// Allocate space for scaled up pixels
	uint32_t *scaled_pixels = malloc(sizeof(uint32_t) * (width * height));

	if (scaled_pixels == NULL)
	{
		strerror_s(error_msg, ERROR_MSG_BUFFER_LEN, errno);
		fprintf(stderr, error_msg);
		exit(EXIT_FAILURE);
	}

	memset(scaled_pixels, 0, sizeof(uint32_t) * width * height);

	*drawSurface = SDL_CreateRGBSurfaceFrom(
		scaled_pixels,
		width,
		height,
		32,
		width * 4,
		0x000000FF,
		0x0000FF00,
		0x00FF0000,
		0x00000000
	);

	if (*drawSurface == NULL)
	{
		printf("Could not create surface: %s\n", SDL_GetError());
		exit(EXIT_FAILURE);
	}
}




