# README #

### What is this repository for? ###

This is a repository for my CHIP-8 CPU emulator. The emulator is written in C. This project was done as a hobby project for me. If anyone else wants to use this emulator they are free to do so.

#### A note on portability ####

As of now the emulator can only run on Windows. The program uses some Microsoft specific C functions, thus it does not compile under other operating systems. If you want to compile the source code yourself please read section on __Compiling the program__. 

### Downloading and installing ###

#### Download

To download the emulator please click on [this link](https://bitbucket.org/patric_gustafsson/chip-8-emulator/downloads/CHIP8_R1111_16.zip). The link contains a zip file that has the executable and necessary .dll files inside. A couple of games are also
included. 

#### Install

It is not necessary to install the program. Just open a command prompt and type `chip8emulator.exe <programname>.<extensions>`. Leave the brackets out when typing in the actual name of the file.

### Compiling the program ###

This program was developed with Visual Studio Community 2015. Visual studio uses Microsoft __Visual C++ 14.0__ as its compiler. This program is not tested with other
compilers but it will probably work with older VS compilers as well. Below follow the instructions how to import the program to Visual Studio and compile it.

#### Importing the program

First download a copy of the repository from [here](https://bitbucket.org/patric_gustafsson/chip-8-emulator/get/0498a27e6e1b.zip), then from Visual Studio open the projects __.sln__ file. Before the program can compile a couple of things need to be setup.

#### Disabling precompiled headers

This project does not use precompiled headers so they need to be disabled in Visual Studio. To disable them for a specific project do the following: 

1. From menu choose __debug then in the drop-down list click on the last item __<projectname> Properties..__
2. Expand the __Configuration Properties list__
3. Expand the __C/C++ list__
4. Click on __Precompiled Headers item__
5. Set as option for __Precompiled Headers__: _Not Using Precompiled Headers_
6. Press save and close the project properties window

#### SDL for Visual Studio

Please follow [this tutorial](http://gigi.nullneuron.net/gigilabs/setting-up-sdl2-with-visual-studio-2015/) to setup SDL in Visual Studio. The process is quite lengthy and too long to include in this README.

The program should now be able to be compiled from Visual Studio.