#ifndef SDLDATA_H
#define SDLDATA_H

#include <SDL.h>

/*
* This structure contains all necessary data for drawing and displaying of windows in this program.
* It simplifes having to pass around many parameters to the draw function and other functions that uses this struct as well
* Can't use SDL_ notation here because it could easily be confused with SDL library function.
*/
struct SDLDATA
{
	SDL_Window *win;
	SDL_Surface *winSurface;
	SDL_Surface *drawSurface;
};

#endif

