#include "chip8.h"
#include <time.h>
#include <io.h>  
#include <math.h>
#include <windows.h>
#include <string.h>

/*
 Conventions used in this code:
 NNN = 12 bit address
 NN = 8 bit constant
 N = 4 bit constant
 X and Y = register identifiers
 VX (VY) = register X or register Y, example: V1 = register one
*/

#define DISPLAY_SIZE 2048
#define ERROR_MSG_BUFFER_LEN 128

/* 
 Functions implementing the opcodes
 The four MSB determine which function to call, then inside function
 the actual opcode is run. Since there can be more than one opcode that have
 the same four most significant bist. 

 Closer documentation in the actual function definition.
*/
void zero_opcode(unsigned short opcode);
void one_opcode(unsigned short opcode);
void two_opcode(unsigned short opcode);
void three_opcode(unsigned short opcode);
void four_opcode(unsigned short opcode);
void five_opcode(unsigned short opcode);
void six_opcode(unsigned short opcode);
void seven_opcode(unsigned short opcode);
void eight_opcode(unsigned short opcode);
void nine_opcode(unsigned short opcode);
void A_opcode(unsigned short opcode);
void B_opcode(unsigned short opcode);
void C_opcode(unsigned short opcode);
void D_opcode(unsigned short opcode);
void E_opcode(unsigned short opcode);
void F_opcode(unsigned short opcode);

// Array of function pointers to instructions
void (*operations[16])(unsigned short opcode);

// Variables representing the CHIP-8 arcitetchture
unsigned char memory[4096];
unsigned char V[16]; // Registers
unsigned short key[16]; // Keyboard flags, if key[x] != 0, then the key is pressed
unsigned short I; // Special index register
unsigned short pc; // The program counter
unsigned char gfx[DISPLAY_SIZE];
unsigned char delay_timer; // Both timers count down at 60hz
unsigned char sound_timer;
unsigned short stack[16];
unsigned short sp; // Stack pointer
bool drawFlag = false, input = false;

// If you align up these hex values by each row
// it's quite easy to see that they form the number they are meant to represent
unsigned char chip8_fontset[80] =
{
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

/* Functions for scaling up of pixels
* src: Pointer to source pixel data to be scaled up
* dst: Pointer to where the new pixel data will be stored
* w1: Width of src data
* h1: Height of src data
* w2: Width of dst data
* h2: Height of dst data
*/
void resize_pixls(unsigned char *src, uint32_t *dst, int w1, int h1, int w2, int h2);

void chip8_init(void)
{
	pc = 0x200; // Program counter starts at 0x200
	I = 0; // Reset index register
	sp = 0; // Reset stack pointer
	delay_timer = 0;
	sound_timer = 0;
	srand(time(NULL));

	// Reset memory
	for (int i = 0; i < 4096; i++)
		memory[i] = 0;

	// Load fontset
	for (int i = 0; i < 80; ++i)
		memory[i] = chip8_fontset[i];

	for (int i = 0; i < DISPLAY_SIZE; i++)
		gfx[i] = 0x00;

	// Reset stack and register data
	for (unsigned short i = 0; i < 16; i++)
	{
		stack[i] = 0;
		V[i] = 0;
	}

	// Load function pointers
	operations[0] = zero_opcode;
	operations[1] = one_opcode;
	operations[2] = two_opcode;
	operations[3] = three_opcode;
	operations[4] = four_opcode;
	operations[5] = five_opcode;
	operations[6] = six_opcode;
	operations[7] = seven_opcode;
	operations[8] = eight_opcode;
	operations[9] = nine_opcode;
	operations[0xA] = A_opcode;
	operations[0xB] = B_opcode;
	operations[0xC] = C_opcode;
	operations[0xD] = D_opcode;
	operations[0xE] = E_opcode;
	operations[0xF] = F_opcode; 

	drawFlag = true;
}

void chip8_load_game(const char *path)
{
	FILE *f = NULL;
	char error_msg[ERROR_MSG_BUFFER_LEN];
	int err = fopen_s(&f, path, "rb");

	if (err != 0)
	{
		strerror_s(error_msg, ERROR_MSG_BUFFER_LEN, errno);
		fprintf(stderr, error_msg);
		exit(EXIT_FAILURE);
	}

	if (f == NULL)
	{
		printf("Could not open the file!\n");
		exit(EXIT_FAILURE);
	}

	/* Since we are reading hex values the number of items that we want to store in the buffer is size / 8. Each 
	* hex value = 1 byte = 8 bits.
	*/
	long fsize = _filelength(_fileno(f)); 

	if (fsize == -1L)
	{
		strerror_s(error_msg, ERROR_MSG_BUFFER_LEN, errno);
		fprintf(stderr, error_msg);
		exit(EXIT_FAILURE);
	}

	printf("Size: %ld\n", fsize);

	uint8_t *buffer = NULL;

	if ((buffer = malloc(sizeof(uint8_t) * fsize)) == NULL)
	{
		strerror_s(error_msg, ERROR_MSG_BUFFER_LEN, errno);
		fprintf(stderr, error_msg);
		exit(EXIT_FAILURE);
	}

	if (fread_s(buffer, (sizeof(uint8_t) * fsize), 1, fsize, f) == 0)
	{
		strerror_s(error_msg, ERROR_MSG_BUFFER_LEN, errno);
		fprintf(stderr, error_msg);
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < fsize ; i++)
		memory[0x200 + i] = buffer[i];
}

void chip8_emulate_cycle(void)
{
	/* Since each address in memory is 1 byte and a opcode is 2 bytes
	 we need to fetch 2 bytes and merge them */
	unsigned short opcode = memory[pc] << 8 | memory[pc + 1];

	drawFlag = false;

	// Determine which operation to use by looking at the four most significant bits, need to shift
	// to get the right array index
	operations[(opcode & 0xF000) >> 12](opcode);

	// Update sound and delay timers
	if (delay_timer > 0)
		--delay_timer;

	if (sound_timer > 0)
	{
		if (sound_timer == 1)
			printf("BEEP!\n");
		--sound_timer;
	}
}

bool chip8_drawflag(void) 
{ 
	return drawFlag; 
}

void chip8_draw(struct SDLDATA *SDLData,
	int orig_width, int orig_height,
	int scaled_width, int scaled_height)
{
	SDL_Window *win = SDLData->win;
	SDL_Surface *winSurface = SDLData->winSurface;
	SDL_Surface *drawSurface = SDLData->drawSurface;

	SDL_LockSurface(drawSurface);
	resize_pixls(gfx, drawSurface->pixels, orig_width, orig_height, scaled_width, scaled_height);
	SDL_UnlockSurface(drawSurface);

	SDL_BlitSurface(drawSurface, NULL, winSurface, NULL);

	SDL_UpdateWindowSurface(win);
}

void resize_pixls(unsigned char *src, uint32_t *dst, int w1, int h1, int w2, int h2)
{
	double x_ratio = w1 / (double) w2;
	double y_ratio = h1 / (double) h2;
	double px, py;
	for (int i = 0; i < h2; i++)
	{
		for (int j = 0; j < w2; j++)
		{
			px = floor(j*x_ratio); // Use floor function to get a integer index
			py = floor(i*y_ratio);
			if (src[(int) ((py*w1) + px)] == 1)
				dst[(i * w2) + j] = 0x00FFFFFF;
			else
				dst[(i * w2) + j] = 0x00000000;
		}
	}
}

void zero_opcode(unsigned short opcode)
{
	switch (opcode & 0x000F) // Decide which opcode is being called by looking at ht elast four bits
	{
		case 0x0000: // Clear the screen	
			for (int i = 0; i < DISPLAY_SIZE; i++)
				gfx[i] = 0x00;
			drawFlag = true;
			pc += 2;
			break;

		case 0x000E: // Return from subroutine
			--sp;
			pc = stack[sp];
			pc += 2;
			break;

		default:
			printf("error: no operation for %X", opcode);
			break;
	}
}

// Jump to address NNN
void one_opcode(unsigned short opcode)
{
	pc = opcode & 0x0FFF; 
}

// Execute subroutine at NNN
void two_opcode(unsigned short opcode)
{
	stack[sp++] = pc;
	pc = opcode & 0x0FFF;
}

// If VX == NN then skip next instruction
void three_opcode(unsigned short opcode)
{
	if (V[(opcode & 0x0F00) >> 8] == (opcode & 0x00FF))
		pc += 4;
	else
		pc += 2;
}

// If VX != NN then skip next instruction
void four_opcode(unsigned short opcode)
{
	if (V[(opcode & 0x0F00) >> 8] != (opcode & 0x00FF))
		pc += 4;
	else
		pc += 2;
}

// If VX == VY then skip next instruction
void five_opcode(unsigned short opcode)
{
	if (V[(opcode & 0x0F00) >> 8] == V[(opcode & 0x00F0) >> 4])
		pc += 4;
	else
		pc += 2;
}

// Set VX to NN
void six_opcode(unsigned short opcode)
{
	V[(opcode & 0x0F00) >> 8] = opcode & 0x00FF;
	pc += 2;
}

// Add to VX the value NN
void seven_opcode(unsigned short opcode)
{
	V[(opcode & 0x0F00) >> 8] += opcode & 0x00FF;
	pc += 2;
}

void eight_opcode(unsigned short opcode)
{
	switch (opcode & 0x000F) // Need to look at last 4 bits to determine which opcode is used
	{
		case 0x0000: // Set VX to the value of VY
			V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0001: // Sets VX to VX | VY
			V[(opcode & 0x0F00) >> 8] |= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0002: // Sets VX to VX & VY
			V[(opcode & 0x0F00) >> 8] &= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0003: // Sets VX to VX ^ VY
			V[(opcode & 0x0F00) >> 8] ^= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0004: // Adds VX to VY, VF is set to 1 if there's a carry else 0
			if (V[(opcode & 0x00F0) >> 4] > (0xFF - V[(opcode & 0x0F00) >> 8]))
				V[0xF] = 1;
			else
				V[0xF] = 0;
			V[(opcode & 0x0F00) >> 8] += V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0005: // Subtracts VY from VX, sets VF to 0 if there's a borrow else 1
			if (V[(opcode & 0x00F0) >> 4] > V[(opcode & 0x0F00) >> 8])
				V[0xF] = 0;
			else
				V[0xF] = 1;
			V[(opcode & 0x0F00) >> 8] -= V[(opcode & 0x00F0) >> 4];
			pc += 2;
			break;

		case 0x0006: // Shifts VX right by one, VF is set to the Least signifcant bit before the shift
			V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x1;
			V[(opcode & 0x0F00) >> 8] >>= 1;
			pc += 2;
			break;

		case 0x0007: // Sets VX to VY - VX, VF is set to 1 if there's a borrow and 0 else
			if (V[(opcode & 0x0F00) >> 8] > V[(opcode & 0x00F0) >> 4])
				V[0xF] = 0;
			else
				V[0xF] = 1;
			V[(opcode & 0x0F00) >> 8] = V[(opcode & 0x00F0) >> 4] - V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x000E: // Shifts VX left by one, VF is set to the most significant bit before the shift
			V[0xF] = V[(opcode & 0x0F00) >> 8] & 0x08;
			V[(opcode & 0x0F00) >> 8] <<= 0x01;
			pc += 2;
			break;

		default:
			printf("error: no operation for %X", opcode);
			break;
	}
}

// If VX != VY then skip next instruction
void nine_opcode(unsigned short opcode)
{
	if (V[(opcode & 0x0F00) >> 8] != V[(opcode & 0x00F0) >> 4])
		pc += 4;
	else
		pc += 2;
}

// Set I to NNN
void A_opcode(unsigned short opcode)
{
	I = opcode & 0x0FFF;
	pc += 2;
}

// Jump to the address NNN + V0
void B_opcode(unsigned short opcode)
{
	pc = (opcode & 0x0FFF) + V[0];
}

// Set VX to a random value that is bitwise ANDed by NN
void C_opcode(unsigned short opcode)
{
	V[(opcode & 0x0F00) >> 8] = (rand() % 0xFF) & (opcode & 0x00FF);
	pc += 2;
}

// Update the graphics
void D_opcode(unsigned short opcode)
{
	unsigned short x = V[(opcode & 0x0F00) >> 8];
	unsigned short y = V[(opcode & 0x00F0) >> 4];
	unsigned short height = opcode & 0x000F;
	unsigned short pixel;
	V[0xF] = 0;

	for (int yline = 0; yline < height; yline++)
	{
		pixel = memory[I + yline];

		// Set the bits for the line (vertically)
		for (int xline = 0; xline < 8; xline++)
		{
			// Check if this pixel is supposed to be set
			if ((pixel & (0x80 >> xline)) != 0)
			{
				// VF need to be set if any bit is set from a SET state to a UNSET state (1 -> 0)
				// coordinate * 64 so we update the right position in the array
				if (gfx[x + xline + ((y + yline) * 64)] == 1)
					V[0xF] = 1;
				gfx[x + xline + ((y + yline) * 64)] ^= 1;
			}
		}
	}

	drawFlag = true;
	pc += 2;
}

void E_opcode(unsigned short opcode)
{
	switch (opcode & 0x00FF)
	{
		case 0x009E: // Skip next instruction if key stored in VX is pressed
			if (key[V[(opcode & 0x0F00) >> 8]] != 0)
				pc += 4;
			else
				pc += 2;
			break;

		case 0x00A1: // Skip next instruction if key stored in VX isn't pressed
			if (key[V[(opcode & 0x0F00) >> 8]] == 0)
				pc += 4;
			else
				pc += 2;
			break;

		default:
			printf("error: no operation for %X", opcode);
			break;
	}
}

void F_opcode(unsigned short opcode)
{
	SDL_Event event;

	switch (opcode & 0x00FF)
	{
		case 0x0007: // Set VX to the value stored in the delay timer
			V[(opcode & 0x0F00) >> 8] = delay_timer;
			pc += 2;
			break;

		case 0x000A: // Wait for key press, store in VX
			input = false;
			while (!input && SDL_WaitEvent(&event))
			{
				switch (event.type)
				{
					case SDL_KEYDOWN:
						V[(opcode & 0x0F00) >> 8] = 1;
						input = true;
						break;
				}
			}
			pc += 2;
			break;

		case 0x0015: // Set the delay timer to VX
			delay_timer = V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x0018: // Set the sound timer to VX
			sound_timer = V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x001E: // Add VX to I
			if (I + V[(opcode & 0x0F00) >> 8] > 0xFF)
				V[0xF] = 1;
			else
				V[0xF] = 0;
			I += V[(opcode & 0x0F00) >> 8];
			pc += 2;
			break;

		case 0x0029: // Sets I to the location of the sprite for the character in VX. Characters 0-F (in hexadecimal) are represented by a 4x5 font.
			I = V[(opcode & 0x0F00) >> 8] * 0x05;
			pc += 2;
			break;

		/*
		Stores the binary-coded decimal representation of VX, with the most significant of three digits at the address in I,
		the middle digit at I plus 1, and the least significant digit at I plus 2.
		(In other words, take the decimal representation of VX, place the hundreds digit in memory at location in I,
		the tens digit at location I+1, and the ones digit at location I+2.)
		*/
		case 0x0033:
			memory[I] = V[(opcode & 0x0F00) >> 8] / 100;
			memory[I + 1] = (V[(opcode & 0x0F00) >> 8] / 10) % 10;
			memory[I + 2] = (V[(opcode & 0x0F00) >> 8] % 100) % 10;
			pc += 2;
			break;

		case 0x0055: // Store V0..VX in memory starting at address I
			for (unsigned short i = 0; i <= ((opcode & 0x0F00) >> 8); i++)
				memory[I + i] = V[0xFF & i];
			pc += 2;
			break;

		case 0x0065: // Fill V0..VX with values from memory statring at adress I
			for (unsigned short i = 0; i <= ((opcode & 0x0F00) >> 8); i++)
				V[0xFF & i] = memory[I + i];
			pc += 2;
			break;

		default:
			printf("error: no operation for %X", opcode);
			break;
	}
}

void chip8_loadkeys(void)
{
	SDL_Event event;

	if (SDL_PollEvent(&event))
	{
		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
				case SDLK_1:
					key[0] = 1;
					break;

				case SDLK_2:
					key[1] = 1;
					break;

				case SDLK_3:
					key[2] = 1;
					break;

				case SDLK_c:
					key[3] = 1;
					break;

				case SDLK_4:
					key[4] = 1;
					break;

				case SDLK_5:
					key[5] = 1;
					break;

				case SDLK_6:
					key[6] = 1;
					break;

				case SDLK_d:
					key[7] = 1;
					break;

				case SDLK_7:
					key[8] = 1;
					break;

				case SDLK_8:
					key[9] = 1;
					break;

				case SDLK_9:
					key[10] = 1;
					break;

				case SDLK_e:
					key[11] = 1;
					break;

				case SDLK_a:
					key[12] = 1;
					break;

				case SDLK_0:
					key[13] = 1;
					break;

				case SDLK_b:
					key[14] = 1;
					break;

				case SDLK_f:
					key[15] = 1;
					break;

				default:
					break;
			}
		}
		else // Reset all keys
		{
			for (int i = 0; i < 16; i++)
				key[i] = 0;
		}
	}
}
