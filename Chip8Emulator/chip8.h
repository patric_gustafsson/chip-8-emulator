#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <SDL.h>
#include "SDLData.h"

/*
* Initializes and resets all the variables related to the emulator
*/
void chip8_init(void);

/*
* Loads a game into the memory
* path: Path to the game to be loaded
*/
void chip8_load_game(const char *path);

/*
* Emulates one CPU cycle. This includes
* - Read next opcode from memory
* - Decode the opcode
* - Execute the opcode
* - Update timers
*/
void chip8_emulate_cycle(void);

/*
* Returns the value of the drawFlag variable. This is used to check if we need to draw
*/
bool chip8_drawflag(void);

/*
* Draws graphics and updates the display
*/
void chip8_draw(struct SDLDATA *SDLData,
	int orig_width, int orig_height,
	int scaled_width, int scaled_height);

/*
* Checks if a key is pressed or unpressed during the current cycle
* Stores that value in the appropriate register
*/
void chip8_loadkeys(void);

